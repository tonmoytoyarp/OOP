<?php

class mobile{
    public $name = "";
    public $brand = "";
    public $price = "";
    public $feedback = "";
    public $ram = "";
    
    
    public function assign ($dat1  = "", $dat2 = "", $dat3 = "", $dat4 = "", $dat5 = "") {
        $this->name = $dat1;
        $this->brand = $dat2;
        $this->price = $dat3;
        $this->feedback = $dat4;
        $this->ram = $dat5;
    }
    
    public function info(){
        echo "The mobile name is : ". $this->name. "</br>";
        echo "The brand name is : ". $this->brand. "</br>";
        echo "The price is : ". $this->price. "<br/>";
        echo "The public feedback is : ". $this->feedback. "<br/>";
        echo "The RAM is : ". $this->ram;
    }
}